/* Copyright 2007, Google Inc. */
/* Copyright (c) 2009, Code Aurora Forum.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <boot/boot.h>
#include <boot/flash.h>
#include <qsd8k/shared.h>

ptentry PTABLE[] = {
    {
        //.start = 360,
        .start = 420,
        .length = 40,
        .name = "boot",
    },
    {
        //.start = 416,
        .start = 476,
        .length = 512,
        .name = "system",
    },
    {
        //.start = 928,
        .start = 988,
        //.length =  95 + 1024,
        .length =  35 + 1024,
        .name = "userdata",
    },
    {
        .name = "",
    },
};

const char *board_cmdline(void) 
{
    return "mem=160M androidboot.console=ttyMSM2 console=ttyMSM2";
}

unsigned board_machtype(void)
{
    return 1008000;  // QSD8X50 SURF
    //return 1008002;  // QSD8X50 FFA
}

void board_init()
{
    unsigned n;
    
    /* if we already have partitions from elsewhere,
    ** don't use the hardcoded ones
    */
    if(flash_get_ptn_count() == 0) {
        for(n = 0; PTABLE[n].name[0]; n++) {
            flash_add_ptn(PTABLE + n);
        }
    }

    clock_enable(UART3_CLK);
    clock_set_rate(UART3_CLK, 19200000 / 4);

    uart_init(2);
}

void board_usb_init(void)
{
}

void board_ulpi_init(void)
{
}

void board_reboot(void)
{
    reboot();
}

void board_getvar(const char *name, char *value)
{
}
